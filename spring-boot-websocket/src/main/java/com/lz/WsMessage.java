package com.lz;
/**
 * websocket通信实体
 *
 * @author: li
 * @date: 2019/1/21
 */
public class WsMessage {
    private String fromName;
    private String content;

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    private String toName;

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
