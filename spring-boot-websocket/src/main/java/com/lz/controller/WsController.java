package com.lz.controller;
/**
 * Websocket接口
 *
 * @author: li
 * @date: 2019/1/21
 */

import com.lz.WsMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class WsController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/addNotice")
    @SendTo("/topic/notice")
    public WsMessage notice(String notice, Principal fromUser) {
        //TODO 业务处理
        WsMessage msg = new WsMessage();
        msg.setFromName(fromUser.getName());
        msg.setContent(notice);
        //向客户端发送广播消息（方式二），客户端订阅消息地址为：/topic/notice //
        messagingTemplate.convertAndSend("/topic/notice", msg);
        return msg;
    }
    @MessageMapping("/msg")
    @SendToUser("/queue/msg/result")
    public boolean sendMsg(WsMessage message, Principal fromUser) {
        //TODO 业务处理
        message.setFromName(fromUser.getName());
        //向指定客户端发送消息，第一个参数Principal.name为前面websocket握手认证通过的用户name（全局唯一的）
        messagingTemplate.convertAndSendToUser(message.getToName(), "/queue/msg/new", message);
        return true;
    }
}
