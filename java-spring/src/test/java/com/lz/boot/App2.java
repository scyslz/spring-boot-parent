package com.lz.boot;

import com.lz.core.beans1.ScanBean01;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * App2..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class App2 {

    @Autowired
    ScanBean01 scanBean01;
    @Test
    public void test() {
        System.out.println(scanBean01.toString());
    }
}
