package com.lz.boot;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * App2..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = {"classpath:cycleDependency.xml"})
public class App3 {

    // @Autowired
    // A a;
    // @Autowired
    // B b;

    @Test
    public void test() {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("classpath:cycleDependency.xml");
        Object a = classPathXmlApplicationContext.getBean("A");
        Object b = classPathXmlApplicationContext.getBean("B");
        System.out.println(a + ":" + b);
    }
}
