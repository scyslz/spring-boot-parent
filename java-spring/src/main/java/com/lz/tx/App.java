package com.lz.tx;

import com.lz.tx.service.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * App..
 *
 * @author Lizhong
 * @date 2019/6/15
 */
public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("tx.xml");
        UserService userService = context.getBean(UserService.class);
        try {

            userService.save();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        // User user = context.getBean(User.class);
        // user.insert();
    }
}
