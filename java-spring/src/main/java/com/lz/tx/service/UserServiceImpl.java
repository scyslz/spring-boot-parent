package com.lz.tx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * UserServiceImpl..
 *
 * @author Lizhong
 * @date 2019/6/18
 */
@Service
public class UserServiceImpl implements UserService {
    /**
     * 创建一张User表
     *
     * 有道spring的笔记
     * PROPAGATION_REQUIRED 	如果当前没有事物，则新建一个事物；如果已经存在一个事物，则加入到这个事物中
     * PROPAGATION_SUPPORTS 	支持当前事物，如果当前没有事物，则以非事物方式执行
     * PROPAGATION_MANDATORY 	使用当前事物，如果当前没有事物，则抛出异常
     * PROPAGATION_REQUIRES_NEW 	新建事物，如果当前已经存在事物，则挂起当前事物
     * PROPAGATION_NOT_SUPPORTED 	以非事物方式执行，如果当前存在事物，则挂起当前事物
     * PROPAGATION_NEVER 	以非事物方式执行，如果当前存在事物，则抛出异常
     * PROPAGATION_NESTED 	如果当前存在事物，则在嵌套事物内执行；如果当前没有事物，则与PROPAGATION_REQUIRED传播特性相同
     *
     * 对于单个方法： 存在异常
     *
     * REQUIRED == REQUIRES_NEW == NESTED 异常会回滚，捕获异常不会回滚
     * SUPPORTS == NOT_SUPPORTED == NEVER  以非事务状态运行
     * MANDATORY == 单个方法没有事务，报错，不能执行
     *
     * 若没有通过代理，则没有事务
     *
     * 对于嵌套：
     * 外                内
     * REQUIRED       REQUIRED      : 外部有异常，或内部有异常，                       捕获异常 依旧 全部回滚
     * REQUIRED       REQUIRES_NEW  : 外部有异常，外部回滚（捕获同NESTED)，            内部不回滚
     * REQUIRED       NESTED        : 内部有异常，外部捕获，                           外部不回滚
     */

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CustomerService customerService;

    String sql = "insert into user (name,age) values ('zhangsan',18) ;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save() {
        jdbcTemplate.execute(sql);
        System.out.println("save");
        customerService.update();
        // int j =1/0;
    }

    @Override

    public void update() {

    }
}
