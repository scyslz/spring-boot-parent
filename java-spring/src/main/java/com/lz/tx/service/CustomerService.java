package com.lz.tx.service;

/**
 * CustomerService..
 *
 * @author Lizhong
 * @date 2019/6/19
 */
public interface CustomerService {
    void save ();
    void update  ();
}
