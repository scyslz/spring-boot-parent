package com.lz.tx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * CustomerServiceImpl..
 *
 * @author Lizhong
 * @date 2019/6/19
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    String sql = "insert into user (name,age) values ('customer',18) ;";

    @Override
    public void save() {

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void update( ) {
        jdbcTemplate.execute(sql);
        System.out.println("update");
        // int j = 1 / 0;
    }
}
