package com.lz.tx.service;
/**
 * UserService..
 *
 * @author Lizhong
 * @date 2019/6/18
 */
public interface UserService {
    void save ();
    void update  ();
}
