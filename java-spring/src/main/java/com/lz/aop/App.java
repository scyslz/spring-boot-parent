package com.lz.aop;

import com.lz.aop.bean.IServiceBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * App..
 *
 * @author Lizhong
 * @date 2019/5/31
 */

@ComponentScan
 @EnableAspectJAutoProxy
public class App {

    public static void main(String[] args) {
         // AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(App.class);
        ClassPathXmlApplicationContext context= new ClassPathXmlApplicationContext("aop-test.xml");

        IServiceBean bean = context.getBean(IServiceBean.class);
        // bean.update();
        bean.insert(110);
        //

        // EnfoceService en = (EnfoceService) bean;
        // en.getAll();
    }}


