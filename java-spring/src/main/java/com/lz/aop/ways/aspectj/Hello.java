package com.lz.aop.ways.aspectj;
/**
 * Hello..
 *
 * @author Lizhong
 * @date 2019/6/11
 */
public class Hello {
    public void hello(String name) {
        System.out.println(name+",hello!");
    }
}
