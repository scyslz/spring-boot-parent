package com.lz.aop.ways.aspectj;

/**
 * Test.. 准备工作
 * 1、idea安装Aspect
 * 2、AspectJ-tools依赖，并切换编译器 AspectJ修改Path
 * 3、注意HelloWorld文件格式
 *
 * @author Lizhong
 * @date 2019/6/11
 */
public class Test {

    public static void main(String[] args) {
        Hello hello = new Hello();
        hello.hello("张三");
    }

    /*OUT:
            hello前的检查,哈哈
            张三,hello!
            hello后的检查，哈哈

     */

}