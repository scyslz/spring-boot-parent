package com.lz.aop.bean;

import org.springframework.stereotype.Service;

/**
 * ServiceBean..
 *
 * @author Lizhong
 * @date 2019/5/31
 */
@Service
public class ServiceBean implements IServiceBean {
    public boolean insert(int i){
        System.out.println("insert:" +i);
        return false;
    }
    public void update(){
        System.out.println("update操作");
    }
}
