package com.lz.aop.bean;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareParents;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * MyAspectJ..
 *
 * @author Lizhong
 * @date 2019/5/31
 */
@Aspect
@Component
public class MyAspectJ {

    // 通过代理，增强目标类方法
    @DeclareParents(value ="com.lz.aop.bean.*" ,defaultImpl = EnfoceServiceImpl.class )
    private EnfoceService s;

    @Pointcut("execution(* com.lz.aop.bean..*.*(..))")
    public void pointcut() {
        System.out.println("我是point");
    }

    @Before("execution(* com.lz.aop.bean..*.update())")
    private void before() {
        System.out.println("我是before");
    }

    @After("pointcut()")
    private void after() {
        System.out.println("我是after");
    }

    /**
     * 配置的前后通知。。。。
     */
    private void before0() {
        System.out.println("我是before0");
    }
    private void after0() {
        System.out.println("我是after0");
    }

}
