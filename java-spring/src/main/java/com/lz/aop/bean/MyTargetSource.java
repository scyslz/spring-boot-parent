package com.lz.aop.bean;

import org.springframework.aop.TargetSource;

/**
 * MyTargetSource..
 *
 * @author Lizhong
 * @date 2019/6/11
 */
public class MyTargetSource implements TargetSource {
    @Override
    public Class<?> getTargetClass() {
        return null;
    }

    @Override
    public boolean isStatic() {
        return false;
    }

    @Override
    public Object getTarget() throws Exception {
        return null;
    }

    @Override
    public void releaseTarget(Object target) throws Exception {

    }
}
