package com.lz.aop.bean;

/**
 * EnfoceService..
 *
 * @author Lizhong
 * @date 2019/5/31
 */
public interface EnfoceService {
    void get();
    void getAll();
}
