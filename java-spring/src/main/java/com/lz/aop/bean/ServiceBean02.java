package com.lz.aop.bean;

import org.springframework.stereotype.Service;

/**
 * ServiceBean..
 *
 * @author Lizhong
 * @date 2019/5/31
 */
@Service
public class ServiceBean02 implements IServiceBean {

    /**
     * 嵌套调用，必须通过这个获取当前代理对象  ( (T)AopContext.currentProxy())，
     * 必须开启                 <aop:aspectj-autoproxy expose-proxy="true" />
     *
     * 删除了注解配置的通知：
     *
     * 我是before0
     * insert操作嵌套Insert
     * update操作
     * 我是after0
     *
     * 我是before0
     * insert操作嵌套Insert
     * 我是before0
     * update操作
     * 我是after0
     * 我是after0
     */
    public boolean insert(int i) {
        System.out.println("insert操作" + "嵌套Insert" + i);

        // 环绕通知 返回没有处理到
        // i = i / 0;

        // update();
        // ( (IServiceBean)AopContext.currentProxy()).update();
        return true;
    }

    public void update() {
        System.out.println("update操作");
    }

}
