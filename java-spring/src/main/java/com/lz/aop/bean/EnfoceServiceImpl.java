package com.lz.aop.bean;

/**
 * EnfoceServiceImpl..
 *
 * @author Lizhong
 * @date 2019/5/31
 */
public class EnfoceServiceImpl implements EnfoceService {
    @Override
    public void get() {
        System.out.println("EnfoceServiceImpl+get");
    }

    @Override
    public void getAll() {
        System.out.println("EnfoceServiceImpl+allGet");
    }
}
