package com.lz.aop.bean;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;

/**
 * MyAdvisor..
 *
 * @author Lizhong
 * @date 2019/6/6
 */
public class MyAdvisor implements Advisor {
    @Override
    public Advice getAdvice() {
        return null;
    }

    @Override
    public boolean isPerInstance() {
        return false;
    }
}
