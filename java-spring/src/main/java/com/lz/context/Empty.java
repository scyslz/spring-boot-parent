package com.lz.context;

import org.springframework.context.annotation.ComponentScan;

/**
 * Empty..
 *
 * @author Lizhong
 * @date 2019/5/10
 */
@ComponentScan
public class Empty {
}
