package com.lz.mvc.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * Student..
 *
 * @author Lizhong
 * @date 2019/6/20
 */
@Getter
@Setter
public class Student {
    private String name;
    private int age ;
    private String [] clazz;
}
