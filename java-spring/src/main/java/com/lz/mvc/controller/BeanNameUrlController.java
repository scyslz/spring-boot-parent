package com.lz.mvc.controller;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * BeanNameUrlController..
 *
 * @author Lizhong
 * @date 2019/6/26
 */
@org.springframework.stereotype.Controller(value = "/url-name-test")
public class BeanNameUrlController implements Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write("Hello:访问ip:" + request.getRemoteAddr());
        return null;
    }
}
