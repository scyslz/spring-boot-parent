package com.lz.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SessionAttributeController..
 *
 * <%
 *     session.setAttribute("name", "张三");
 * %>
 *
 * @author Lizhong
 * @date 2019/6/27
 */

@SessionAttributes(names = "name")
@Controller
@RequestMapping("/session")
public class SessionAttributeController {

    @RequestMapping(value = "/hello/a", method = RequestMethod.GET)
    public String method02(
            HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

      /*  try {
            httpServletResponse.setContentType("text/html;charset=UTF-8");
            httpServletResponse.getWriter().write(name + ":访问ip:" + httpServletRequest.getRemoteAddr());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return "index";
    }

}
