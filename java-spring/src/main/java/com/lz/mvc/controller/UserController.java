package com.lz.mvc.controller;

import com.lz.mvc.bean.Student;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * UserController..
 *
 * @author Lizhong
 * @date 2019/6/20
 */
@RequestMapping("/user")
@Controller
public class UserController {


    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ModelAndView method01(@RequestParam(required = false) String name, @RequestParam(required = false) int age,
                                 HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                 ModelAndView modelAndView) {

        modelAndView.addObject("name", "张三" + name);
        modelAndView.addObject("age", age);
        modelAndView.setViewName("index");
        // 必须返回一个ModelView
        return modelAndView;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.POST,
            produces = MediaType.ALL_VALUE, consumes = MediaType.ALL_VALUE)
    public void method0a(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         // 将RequestParam封装JavaBean
                         @ModelAttribute() Student student) {

        try {
            httpServletResponse.setContentType("text/html;charset=UTF-8");
            httpServletResponse.getWriter().write(student.getName() + student.getAge() + ":访问ip:" + httpServletRequest.getRemoteAddr());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/hello/a", method = RequestMethod.GET)
    public String method02(
            // jsp 传过来装入ModelAttribute
            @SessionAttribute(required = false) String name,
            HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

      /*  try {
            httpServletResponse.setContentType("text/html;charset=UTF-8");
            httpServletResponse.getWriter().write(name + ":访问ip:" + httpServletRequest.getRemoteAddr());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return "index";
    }

    /**
     * ModelAndView,其实就是转发，Model添加到 requestScope域中
     * @param name
     * @param httpServletRequest
     * @param httpServletResponse
     */
    @RequestMapping(value = "/hello/b", method = RequestMethod.GET)
    public void method03(@RequestParam(required = false) String name,
                         HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        // 模拟model
        Student o = new Student();
        o.setAge(100);
        o.setName(name);
        httpServletRequest.setAttribute("student0", o);
        try {
            httpServletRequest.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(httpServletRequest,httpServletResponse);
        } catch (ServletException e) {


        } catch (IOException e) {
            e.printStackTrace();
        }
        // 打印出域对象中的attribute。。。
        /*
        Enumeration<String> attributeNames = httpServletRequest.getSession().getAttributeNames();
        System.out.println("session---");
        cout(attributeNames);
        System.out.println("request---");
        Enumeration<String> attributeNames2 = httpServletRequest.getAttributeNames();
        cout(attributeNames2);
        System.out.println("context---");
        Enumeration<String> attributeNames1 = httpServletRequest.getServletContext().getAttributeNames();
        cout(attributeNames1);
        */

        return ;
    }

    private void cout(Enumeration<String> attributeNames) {
        while(attributeNames.hasMoreElements()){
            String s = attributeNames.nextElement();
            System.out.println(s);
        }
    }


    @RequestMapping(value = "/hello/json", method = RequestMethod.GET,
            produces = MediaType.ALL_VALUE, consumes = MediaType.ALL_VALUE)
    @ResponseBody
    public String method0Json(HttpServletRequest httpServletRequest,
                              HttpServletResponse httpServletResponse) {
        return "hello";

    }

    /**
     * https://www.helplib.com/Java_API_Classes/article_70309 WebBindData
     * @param dataBinder
     */
    @InitBinder
    public void initBinderTypeConversion(WebDataBinder dataBinder) {
        // dataBinder.setDisallowedFields("name");
        // 1、参数转为ModelAttribute
        dataBinder.setAllowedFields("name", "age", "clazz");
        // 2、自定义Format
     /*   dataBinder.addCustomFormatter(new Formatter<Object>() {
            @Override
            public Object parse(String text, Locale locale) throws ParseException {
                return text + ":parse";
            }

            @Override
            public String print(Object object, Locale locale) {
                return object + ":print";
            }
        });*/
    }

    /**
     * 请求进来，优先执行这个方法
     */
    @ModelAttribute(name = "student0")
    public Student method01(Student student) {
        return student;
    }

   /* @ModelAttribute(name = "student", binding = false)
    public void method02(Student student, Model model) {
        model.addAttribute(student);
    }*/


}
