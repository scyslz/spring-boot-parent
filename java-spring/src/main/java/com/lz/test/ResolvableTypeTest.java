package com.lz.test;

import org.springframework.core.ResolvableType;

import java.util.HashMap;
import java.util.List;

/**
 * sdf..
 *
 * 获取泛型接口
 *
 * @author Lizhong
 * @date 2019/6/21
 */
public class ResolvableTypeTest {
    private HashMap<Integer, List<String>> myMap;

    public void example() {
        ResolvableType t = null;
        try {
            t = ResolvableType.forField(getClass().getDeclaredField("myMap"));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        ResolvableType superType = t.getSuperType();// AbstractMap&lt;Integer, List&lt;String&gt;&gt;
        ResolvableType resolvableType = t.asMap();// Map&lt;Integer, List&lt;String&gt;&gt;
        Class<?> resolve = t.getGeneric(0).resolve();// Integer
        Class<?> resolve1 = t.getGeneric(1).resolve();// List
        ResolvableType generic = t.getGeneric(1);// List&lt;String&gt;
        Class<?> aClass = t.resolveGeneric(1, 0);// String
    }

    public static void main(String[] args) {
        ResolvableTypeTest resolvableTypeTest = new ResolvableTypeTest();
        resolvableTypeTest.example();
    }
}
