package com.lz.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

/**
 * FirstDemo..
 *
 * @author Lizhong
 * @date 2019/5/6
 */
// @Configuration
public class FirstDemo {

}

class App {
    public static void main(String[] args) {
        Annotation[] annotations = FirstDemo.class.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation.toString());
            System.out.println(annotation.annotationType());
            System.out.println(annotation.getClass().getName());
            System.out.println(annotation.getClass().getTypeName());
            System.out.println(annotation.getClass().toString());
        }

        if (FirstDemo.class.isAnnotationPresent(Component.class)) {
            System.out.println("find Component annotation");


            Annotation annotation = FirstDemo.class.getAnnotation(Component.class);

            System.out.println(annotation.toString());
            System.out.println(annotation.annotationType());
            System.out.println(annotation.getClass().getName());
            System.out.println(annotation.getClass().getTypeName());
            System.out.println(annotation.getClass().toString());
        }
    }
}

