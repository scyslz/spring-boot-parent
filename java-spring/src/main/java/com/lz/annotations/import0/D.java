package com.lz.annotations.import0;
/**
 * A..
 *
 * @author Lizhong
 * @date 2019/5/10
 */
public class D {
    private String name="DdefaultName";
    private int  age=0;

    @Override
    public String toString() {
        return "D{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
