package com.lz.annotations.import0;

import org.springframework.core.type.AnnotationMetadata;

/**
 * MyImportSelector..
 *
 * @author Lizhong
 * @date 2019/5/10
 */
public class MyImportSelector implements org.springframework.context.annotation.ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        // 导入C
        return new String[]{"com.lz.annotations.import0.C"};
    }
}
