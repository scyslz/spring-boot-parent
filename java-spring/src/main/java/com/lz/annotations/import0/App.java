package com.lz.annotations.import0;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;

/**
 * App.. 注解@Import三种使用实例
 *
 * @see {@link org.springframework.context.annotation.ConfigurationClassPostProcessor} PostProcessor中处理已经注册的Bean，入口
 * @see {@link org.springframework.context.annotation.ConfigurationClassParser}        解析
 * @see {@link org.springframework.context.annotation.ConfigurationClassBeanDefinitionReader} 创建
 *
 * {@link org.springframework.context.annotation.ConfigurationClassUtils}
 *
 * @author Lizhong
 * @date 2019/5/10
 */
// 第一种用法
// @Import({A.class, B.class})
// 第二种 实现 ImportSelector
// @Import({A.class, B.class,MyImportSelector.class})
// 第三种 实现MyImportBeanDefinitionRegistrar
@Import({A.class, B.class,MyImportSelector.class,MyImportBeanDefinitionRegistrar.class})
public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(App.class);
        String[] beanDefinitionNames = annotationConfigApplicationContext.getBeanDefinitionNames();
        C bean = annotationConfigApplicationContext.getBean(C.class);
        System.out.println(bean);
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
    }
}
