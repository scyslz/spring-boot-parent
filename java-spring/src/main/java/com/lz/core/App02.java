package com.lz.core;

import com.lz.core.beans.MyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * App02..
 * GenericApplicationContext 加载手动refresh
 *
 * @author Lizhong
 * @date 2019/5/14
 */


public class App02 {
    public static void main(String[] args) {

        GenericApplicationContext ctx = new GenericApplicationContext();

        XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(ctx);
        // Location在AbstractBeanDefinitionReader
        // xmlReader.loadBeanDefinitions("applicationContext.xml");

        // XmlBeanDefinitionReader
        xmlReader.loadBeanDefinitions(new ClassPathResource("applicationContext.xml"));
        // PropertiesBeanDefinitionReader propReader = new PropertiesBeanDefinitionReader(ctx);
        // propReader.loadBeanDefinitions(new
        //
        //         ClassPathResource("otherBeans.properties"));
        ctx.addBeanFactoryPostProcessor(new BeanDefinitionRegistryPostProcessor() {
            @Override
            public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
                System.out.println("编程式#postProcessBeanDefinitionRegistry");
            }

            @Override
            public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
                System.out.println("编程式#postProcessBeanFactory");
            }
        });
        ctx.refresh();
        MyFactoryBean bean = ctx.getBean(MyFactoryBean.class);
        System.out.println(bean);

    }

}
