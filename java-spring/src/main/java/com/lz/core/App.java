package com.lz.core;

import com.lz.core.beans.Bean01;
import com.lz.core.beans.MyFactoryBean;
import com.lz.core.beans.Student;
import com.lz.core.beans1.ScanBean01;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

/**
 * App..main中通过xml方式加载上下文
 *
 * @author Lizhong
 * @date 2019/5/7
 */
// @ContextConfiguration
public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{"classpath:applicationContext.xml"});
        // ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{"classpath:*"},false);
        Student bean = classPathXmlApplicationContext.getBean(Student.class);
        System.out.println(bean.getLessons());

        // 获取MyFactoryBean
        Object myFactoryBean3 = classPathXmlApplicationContext.getBean("myFactoryBean");
        Object myFactoryBean1 = classPathXmlApplicationContext.getBean(MyFactoryBean.class);
        Object myFactoryBean2 = classPathXmlApplicationContext.getBean("&myFactoryBean");
        System.out.println(myFactoryBean1);
        System.out.println(myFactoryBean2);
        System.out.println(myFactoryBean3);

        try {
            System.out.println(((MyFactoryBean) myFactoryBean2).getObject());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 默认FactoryBean，没有factory
/*        Object bean1 = classPathXmlApplicationContext.getBean("&lesson03");
        System.out.println("默认Factory："+bean1);*/
        String[] beanDefinitionNames = classPathXmlApplicationContext.getBeanDefinitionNames();
        System.out.println(beanDefinitionNames.length);

        System.out.println("==============================");
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println("AllBeanName:"+beanDefinitionName);
        }
        System.out.println("==============================");
        Object repeatNameTest = classPathXmlApplicationContext.getBean(Bean01.class);
        System.out.println(repeatNameTest);
        // 自定义BeanName Generator
        System.out.println(Arrays.toString(classPathXmlApplicationContext.getBeanNamesForType(ScanBean01.class)));

        // 打印所有Bean

    }

}
