package com.lz.core.scan;

import org.springframework.stereotype.Component;

/**
 * BeanSacn..
 *
 * @author Lizhong
 * @date 2019/5/20
 */
@Component
public class BeanSacn {
    private String name =BeanSacn.class.getName();

    @Override
    public String toString() {
        return "BeanSacn{" +
                "name='" + name + '\'' +
                '}';
    }
}
