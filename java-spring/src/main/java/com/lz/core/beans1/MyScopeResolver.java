package com.lz.core.beans1;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ScopeMetadata;
import org.springframework.context.annotation.ScopeMetadataResolver;
import org.springframework.context.annotation.ScopedProxyMode;

/**
 * MyScopeResolver..
 *
 * 二选一 Scope-Proxy 模式，interface，target-class，no
 *
 * @author Lizhong
 * @date 2019/5/20
 */
public class MyScopeResolver implements ScopeMetadataResolver {
    /**
     * 参考实现 {@link org.springframework.context.annotation.AnnotationScopeMetadataResolver#resolveScopeMetadata(BeanDefinition)}
     * @param definition
     * @return
     */
    @Override
    public ScopeMetadata resolveScopeMetadata(BeanDefinition definition) {
        ScopeMetadata metadata = new ScopeMetadata();
        metadata.setScopeName(BeanDefinition.SCOPE_SINGLETON);
        metadata.setScopedProxyMode(ScopedProxyMode.DEFAULT);
        return metadata;
    }
}
