package com.lz.core.beans1;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;

/**
 * MyNameGenerator..
 *
 * @author Lizhong
 * @date 2019/5/20
 */
public class MyNameGenerator implements BeanNameGenerator {
    @Override
    public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
        String s = definition.getBeanClassName() + ":zl";
        return s;
    }
}
