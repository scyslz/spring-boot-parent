package com.lz.core.beans1;

import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;

/**
 * MyAnnotationFilter..
 *
 * @author Lizhong
 * @date 2019/5/20
 */
public class MyAnnotationFilter extends AnnotationTypeFilter {
    public MyAnnotationFilter(Class<? extends Annotation> annotationType) {
        super(annotationType);
    }

    public MyAnnotationFilter(Class<? extends Annotation> annotationType, boolean considerMetaAnnotations) {
        super(annotationType, considerMetaAnnotations);
    }

    public MyAnnotationFilter(Class<? extends Annotation> annotationType, boolean considerMetaAnnotations, boolean considerInterfaces) {
        super(annotationType, considerMetaAnnotations, considerInterfaces);
    }
}
