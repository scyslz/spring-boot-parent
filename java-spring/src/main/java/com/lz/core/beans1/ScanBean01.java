package com.lz.core.beans1;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Indexed;

/**
 * ScanBean01..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
@Configuration
@Indexed()
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ScanBean01 {

    private String name="scan01";

    @Override
    public String toString() {
        return "ScanBean01{" +
                "name='" + name + '\'' +
                '}';
    }
}
