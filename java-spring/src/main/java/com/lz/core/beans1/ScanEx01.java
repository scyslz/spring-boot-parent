package com.lz.core.beans1;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Indexed;

/**
 * ScanBean01..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
@Configuration
@Indexed()
public class ScanEx01 {

    private String name="ex01";

    @Override
    public String toString() {
        return "ex01{" +
                "name='" + name + '\'' +
                '}';
    }
}
