package com.lz.core.beans1;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Indexed;

/**
 * ScanBean01..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
@Configuration
@Indexed()
public class ScanBean02 {

    private String name="scan02";

    @Override
    public String toString() {
        return "ScanBean02{" +
                "name='" + name + '\'' +
                '}';
    }
}
