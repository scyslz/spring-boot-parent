package com.lz.core.beans;
/**
 * BeanConstant..
 *
 * @author Lizhong
 * @date 2019/5/22
 */
public class BeanConstant {
    private String name="zl";
    private int age=52;
    private int number=100;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
