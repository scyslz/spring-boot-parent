package com.lz.core.beans;

/**
 * Bean07..
 *
 *
 * 测试  <bean class="com.lz.core.beans.Bean07">
 *         <property name="student.lesson.name" value=".Chinese"/>
 *     </bean>
 *
 * @author Lizhong
 * @date 2019/5/24
 */
public class Bean07 {
    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Bean07{" +
                "student=" + student +
                '}';
    }
}
