package com.lz.core.beans.cycledependency;
/**
 * A..
 *
 * @author Lizhong
 * @date 2019/5/27
 */
public class A {
    private  B b;

    public A() {
    }

    public A(B b) {
        this.b = b;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }
}
