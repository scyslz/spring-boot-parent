package com.lz.core.beans;

import org.springframework.beans.factory.FactoryBean;

/**
 * MyFactoryBean..
 * @see org.springframework.beans.factory.FactoryBean 仅仅spring容器控制某些对象的创建方式
 * @see org.springframework.beans.factory.BeanFactory 管理整个spring容器，创建容器之前的Factory Xml、管理所有Bean...
 *
 * 创建了MyFactoryBean对象，还创建了个MyFactoryBean#getObject对象
 *
 * getBean(Class zz)    得到的是工厂
 * getBean(&byName)     得到的是工厂
 * getBean(byName)      得到是工厂#getObject
 *
 *
 * @author Lizhong
 * @date 2019/5/8
 */
public class MyFactoryBean implements FactoryBean {
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Object getObject() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setName("我是MyFactoryBean创建");
        return lesson;
    }

    @Override
    public Class<?> getObjectType() {
        return Lesson.class;
    }
}
