package com.lz.core.beans;

import org.springframework.beans.factory.BeanNameAware;

/**
 * Bean04BeanNameAware..
 *
 * @author Lizhong
 * @date 2019/5/10
 */
public class Bean04BeanNameAware implements BeanNameAware {
    private String id;

    private String name ;
    private int age;
    @Override
    public void setBeanName(String name) {
        this.id=name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Bean04BeanNameAware{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
