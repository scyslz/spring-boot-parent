package com.lz.core.beans.lookup;

/**
 * GetedBean06..
 *
 * @author Lizhong
 * @date 2019/5/16
 */
public abstract class GetBean06 {
    /**
     * 用于lookup-method
     * @return Bean06Parent
     */
    public abstract Bean06Parent getBean06();

    /**
     *
     用于测试replace-method，用replace.Replacer.class
     */
    public void changedMethod() {
        System.out.println("Origin method in MyTestBean run");
    }
}
