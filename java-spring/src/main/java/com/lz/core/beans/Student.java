package com.lz.core.beans;

import java.util.List;

/**
 * Person..
 *
 * @author Lizhong
 * @date 2019/5/7
 */

public class Student {
    private String name;
    private String addr;
    private List lessons;
    private Lesson lesson;

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public List getLessons() {
        return lessons;
    }

    public void setLessons(List lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", lessons=" + lessons +
                ", lesson=" + lesson +
                '}';
    }
}
