package com.lz.core.beans.lookup;
/**
 * Bean06Parent..
 *
 * @author Lizhong
 * @date 2019/5/16
 */
public  class Bean06Parent {
    /**
     * 假设一个单例模式的bean A需要引用另外一个非单例模式的bean B，为了在我们每次引用的时候都能拿到最新的bean B，
     * 我们可以让bean A通过实现ApplicationContextWare来感知applicationContext（即可以获得容器上下文），
     * 从而能在运行时通过ApplicationContext.getBean(String beanName)的方法来获取最新的bean B。
     * 但是如果用ApplicationContextAware接口，就让我们与Spring代码耦合了，违背了反转控制原则（IoC，
     * 即bean完全由Spring容器管理，我们自己的代码只需要用bean就可以了）。
     */
}
