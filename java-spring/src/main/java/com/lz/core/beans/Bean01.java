package com.lz.core.beans;


import java.util.List;

/**
 * Bean01..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
public class Bean01 {
    private String id;
    private String name;
    private int age;
    private List lessons;

    public Bean01(String name, int age,List<Lesson> lessons) {
        this.name = name;
        this.age = age;
        this.lessons=lessons;
    }

    @Override
    public String toString() {
        return "Bean01{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", lessons=" + lessons +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
