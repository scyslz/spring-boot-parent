package com.lz.core.beans.cycledependency;

/**
 * A..
 *
 * @author Lizhong
 * @date 2019/5/27
 */
public class B {
    private A a;

    public B(A a) {
        this.a = a;
    }

    public B() {
    }

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }
}
