package com.lz.core.beans.constructor;
/**
 * ConstructorIndex..
 *
 * @author Lizhong
 * @date 2019/5/17
 */
public class ConstructorIndex {
    private int n1;
    private int n2;
    private int n3;

    public ConstructorIndex(int n1, int n2, int n3, int n4) {
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
        this.n4 = n4;
    }

    private int n4;

    @Override
    public String toString() {
        return "ConstructorIndex{" +
                "n1=" + n1 +
                ", n2=" + n2 +
                ", n3=" + n3 +
                ", n4=" + n4 +
                '}';
    }
}
