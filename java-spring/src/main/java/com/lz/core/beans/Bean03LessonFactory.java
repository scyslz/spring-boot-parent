package com.lz.core.beans;

import org.apache.commons.lang3.RandomUtils;

/**
 * Bean03LessonFactory..
 *
 * @author Lizhong
 * @date 2019/5/7
 */
public class Bean03LessonFactory {
    public static Lesson getLesson() {
        Lesson lesson = new Lesson();
        lesson.setName("静态工厂Factory Lesson");
        lesson.setNumber(RandomUtils.nextInt(0,100));
        return lesson;
    }
    public  Lesson getLessonImpl() {
        Lesson lesson = new Lesson();
        lesson.setName("实例化Factory  Lesson");
        lesson.setNumber(RandomUtils.nextInt(0,100));
        return lesson;
    }
}
