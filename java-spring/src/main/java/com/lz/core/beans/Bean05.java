package com.lz.core.beans;
/**
 * Bean05..
 *
 * @author Lizhong
 * @date 2019/5/16
 */
public class Bean05   {
    private String name;
    public Bean05( String name ,int number) {
      this.name=name;
      this.number=number;
    }
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Bean05{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }
}
