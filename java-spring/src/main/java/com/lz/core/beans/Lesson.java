package com.lz.core.beans;
/**
 * Lesson..
 *
 * @author Lizhong
 * @date 2019/5/7
 */

public class Lesson{
    private String  name;
    private int number;
    public  int score;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }
}
