<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insert title here</title></head>
<body> sayHello...<br/>
<h1>name...${name}</h1>
<h1>name1...${name1}</h1>
<h2>...${age}</h2>

<%--有前端传给session,用于@SessionAttribute 和 @SessionAttributes--%>
<%
    session.setAttribute("name", "张三");
%>
<%--反过来再取出来--%>
<div> seession [ name: ${sessionScope.get("name")}],${sessionScope.size()}</div>
<div>model:student :${student0.name} ，age: ${student0.age} ,class :${student0.clazz[0]},${student0.clazz[1]}</div>
<div>request:student :${requestScope.student0.name} ，age: ${requestScope.student0.age} ,class :${requestScope.student0.clazz[0]},${student0.clazz[1]}</div>

<div>${applicationScope.size()}</div>
<div>${requestScope.size()}</div>
<div>${sessionScope.size()}</div>
<div>${pageScope.size()}</div>
</body>
</html>