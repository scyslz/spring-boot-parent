package com.lz;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

@EnableAutoConfiguration
public class DemoApplication {
	public static void main(String[] args) {
		// SpringApplication.run(DemoApplication.class, args);
		SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder(DemoApplication.class);
		springApplicationBuilder.web(WebApplicationType.NONE);
		ConfigurableApplicationContext context = springApplicationBuilder.run(args);
		// ObjectMapper bean = context.getBean(ObjectMapper.class);
		// if(bean==null) throw new NoSuchBeanDefinitionException("000");
		// else System.out.println(bean);
		Map<String, Formatter> beansOfType = context.getBeansOfType(Formatter.class);
		Integer i=123;
		beansOfType.forEach((k,v)->{

			String format = v.format(i);
			System.out.println(k +":"+format);
		});

	context.close();

	}


}
