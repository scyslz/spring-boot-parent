package com.zl.s;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * @description
 *
 * @date 2021-08-13 16:23
 *
 * @author Lizhong
 */
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public void save(User user) {
        userRepository.save(user);

    }

    public User getById(Long id) {
        return userRepository.findById(id).get();

    }

    public boolean updateById(User user) {
        return true;
    }

    public boolean removeById(Long id) {
        return true;
    }

    public Page<User> list() {
        PageRequest of = PageRequest.of(1, 10, Sort.by("num"));
        return userRepository.findAll(of);
    }

    public int count() {
        return 0;
    }
}
