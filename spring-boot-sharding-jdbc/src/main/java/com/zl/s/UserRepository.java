package com.zl.s;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description
 *
 * @date 2021-08-13 16:24
 *
 * @author Lizhong
 */
public interface UserRepository extends JpaRepository<User,Long> {
}
