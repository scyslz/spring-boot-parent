package com.zl;

import com.zl.this0.AsyncDemo;
import com.zl.this0.Mixed;
import com.zl.this0.TxDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

/**
 * -javaagent:C:\Users\DELL\.m2\repository\org\springframework\spring-instrument\4.2.5.RELEASE\spring-instrument-4.2.5.RELEASE.jar
 *
 *  -javaagent:C:\Users\DELL\.m2\repository\org\aspectj\aspectjweaver\1.8.8\aspectjweaver-1.8.8.jar
 *
 * @date 2021/6/28 11:00
 * @author Lizhong
 */
@SpringBootApplication
// @EnableAsync(mode = AdviceMode.ASPECTJ)
@EnableAsync(proxyTargetClass = true)
@EnableAspectJAutoProxy(proxyTargetClass = true,exposeProxy = true)
@RestController
public class Application {
    @Autowired
    AsyncDemo asyncDemo;

    @Autowired
    Mixed mixed
            ;

    public static void main(String[] args) throws InterruptedException {
        SpringApplication springApplication = new SpringApplication(Application.class);
        springApplication.setWebApplicationType(WebApplicationType.SERVLET);
        ConfigurableApplicationContext run = springApplication.run(args);
        TxDemo bean = run.getBean(TxDemo.class);
        // bean.hello2();

        Thread.sleep(3000);

    }

    /**
     * 使用AspectJ时 postConstruct可能获取的不是代理对象
     */
    @GetMapping("/")
    @PostConstruct
    public void test() {
        asyncDemo.asyn002();
        System.out.println("tx================================================================");
        mixed.tx();
    }


}
