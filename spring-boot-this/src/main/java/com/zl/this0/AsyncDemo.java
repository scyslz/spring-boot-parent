package com.zl.this0;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 *
 * @date 2021/6/28 14:50
 * @author Lizhong
 */
@Service
public class AsyncDemo {

    @Autowired
    ApplicationContext applicationContext;
    @Async
    public void asyn001() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
        //TODO 2021/6/28 14:29
    }

    @Async
    public void asyn00() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
        //TODO 2021/6/28 14:29
    }

    @Async
    public void asyn002() {
        System.out.println(Thread.currentThread().getName());
        asyn00();

        AsyncDemo bean = applicationContext.getBean(AsyncDemo.class);
        bean.asyn001();

    }
}
