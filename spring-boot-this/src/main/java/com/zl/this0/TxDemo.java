package com.zl.this0;

import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 *
 *
 * @date 2021/6/28 11:01
 * @author Lizhong
 */
@Service
public class TxDemo {
    @Autowired
    ApplicationContext applicationContext;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void hello1() {

        System.out.println(TransactionAspectSupport.currentTransactionStatus());
    }

    @Transactional
    public void hello2() {
        System.out.println(Thread.currentThread().getId());
        System.out.println(TransactionAspectSupport.currentTransactionStatus());
        TxDemo o = (TxDemo) AopContext.currentProxy();
        o.hello1();
        TxDemo bean = applicationContext.getBean(TxDemo.class);
        System.out.println(o == bean);


    }



}
