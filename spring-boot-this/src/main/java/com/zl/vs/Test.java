package com.zl.vs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

/**
 *
 *
 * @date 2021/6/28 16:05
 * @author Lizhong
 */
@RestController
public class Test {
    @Autowired
    ConfigurationDemoFull configurationDemoFull;

    @Autowired
    ConfigurationDemoLite configurationDemoLite;

    @Autowired
    ComponentFullDemo componentFullDemo;

    @GetMapping("/bean")
    @PostConstruct
    public void  aVoid(){
        Object f1 = configurationDemoFull.create();
        Object f2 = configurationDemoFull.create();
        System.out.println(f1==f2); // true

        Object l1 = configurationDemoLite.create();
        Object l2 = configurationDemoLite.create();
        System.out.println(l1==l2);// false

        Object c1 = componentFullDemo.create();
        Object c2 = componentFullDemo.create();
        System.out.println(c1==c2);// false
    }
}
