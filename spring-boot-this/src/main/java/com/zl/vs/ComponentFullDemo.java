package com.zl.vs;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 *
 *
 * @date 2021/6/28 16:04
 * @author Lizhong
 */
@Component
public class ComponentFullDemo {
    @Bean("User03")
    public User create(){
        return new User();
    }
}
