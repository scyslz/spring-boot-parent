package com.zl.vs;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 *
 *
 * @date 2021/6/28 16:28
 * @author Lizhong
 */
@Component
public class UserPostProcesssor implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof  User){
            System.out.println("postProcessAfterInitialization");
        }
        return bean;
    }


}
