package com.zl.vs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 * @date 2021/6/28 15:51
 * @author Lizhong
 */
@Configuration(proxyBeanMethods = false)
public class ConfigurationDemoLite {
    @Bean("User02")
    public User create(){
        return new User();
    }


}
