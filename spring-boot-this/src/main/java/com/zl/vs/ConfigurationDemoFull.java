package com.zl.vs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 * @date 2021/6/28 15:51
 * @author Lizhong
 */
@Configuration
public class ConfigurationDemoFull {
    @Bean("User01")
    public User create(){
        return new User();
    }


}
