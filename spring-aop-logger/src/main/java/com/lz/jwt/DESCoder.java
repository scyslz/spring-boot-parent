package com.lz.jwt;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.Key;

public class DESCoder {
    private Key toKey(byte[] key) throws Exception {
        DESKeySpec dks = new DESKeySpec(key);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DESBuilder.KEY_ALGORTHM);
        SecretKey secretKey = keyFactory.generateSecret(dks);
        return secretKey;
    }

    /**
     * @param key key(Base64编码）
     * @return
     * @throws Exception
     */
    public Key toKey(String key) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(key);
        Key keyObj = toKey(keyBytes);
        return keyObj;
    }
}
