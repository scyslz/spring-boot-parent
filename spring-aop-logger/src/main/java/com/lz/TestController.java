package com.lz;

import com.lz.annotation.AuditLog;
import com.lz.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@AuditLog
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping("/")
    public String hello(){
        return testService.sayhello();
    }

    @GetMapping("/2")
    public String goodbye(){
        return testService.sayGoodBye();
    }



}
