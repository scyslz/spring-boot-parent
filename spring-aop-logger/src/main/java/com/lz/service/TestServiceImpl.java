package com.lz.service;

import com.lz.annotation.AuditLog;
import org.springframework.stereotype.Service;

import java.util.Random;

@AuditLog
@Service
public class TestServiceImpl implements TestService {
    @Override
    public String sayhello() {
        Random random = new Random();
        if(random.nextBoolean()) throw new RuntimeException("模拟异常");

        return "success";
    }

    @Override
    public String sayGoodBye() {
        return "good bye";
    }
}
