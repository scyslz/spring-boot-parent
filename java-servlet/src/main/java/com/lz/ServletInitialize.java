package com.lz;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

/**
 * ServletInitialize.. maven打包成功
 *
 * @author Lizhong
 * @date 2019/4/9
 */
public class ServletInitialize implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        System.out.println("ServletInitialize begin ****");
        ServletRegistration.Dynamic program = servletContext.addServlet("program", ProgramServlet.class);
        program.addMapping("/program");
    }
}
