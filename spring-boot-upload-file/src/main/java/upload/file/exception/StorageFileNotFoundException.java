package upload.file.exception;

import java.net.MalformedURLException;

public class StorageFileNotFoundException extends Exception {
    public StorageFileNotFoundException(String s, MalformedURLException e) {
        super(s,e);
    }

    public StorageFileNotFoundException(String s) {
        super(s);
    }
}
