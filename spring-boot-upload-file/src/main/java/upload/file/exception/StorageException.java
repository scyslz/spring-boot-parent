package upload.file.exception;
/**
 * @Classname: StorageException
 * @Description: StorageException
 *
 * @Date: 2019/1/7 14:37
 * @Created by: li
 * @Version: V1.0
 */
public class StorageException extends Exception {
    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
