package upload.file.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Classname: uploadfile.fc
 * @Description: 获取到前缀
 *
 * @Date: 2019/1/7 14:30
 * @Created by: li
 * @Version: V1.0
 */


// @Component
@ConfigurationProperties(value = "storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = "upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}