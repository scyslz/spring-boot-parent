package upload.file.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import upload.file.Application;

/**
 * 打包war的配置文件.
 *
 * @author: li
 * @date: 2019/1/19
 */
public class PackageWar extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }
}
