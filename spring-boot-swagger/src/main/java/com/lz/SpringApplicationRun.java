package com.lz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * SpringApplicationRun..
 *
 * @author Lizhong
 * @date 2019/7/12
 */
@SpringBootApplication
@EnableSwagger2
public class SpringApplicationRun {
    public static void main(String[] args) {
        SpringApplication.run(SpringApplicationRun.class, args);
    }
}
