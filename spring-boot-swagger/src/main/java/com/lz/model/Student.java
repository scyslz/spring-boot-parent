package com.lz.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Student..
 *
 * @author Lizhong
 * @date 2019/7/12
 */
@ApiModel
@Data
public class Student {
    @ApiModelProperty(example = "name")
    private String name;
    @ApiModelProperty(example = "11")
    private Integer age1;
    // @ApiModelProperty(example = "12")
    private Long age2;
    @ApiModelProperty(example = "21")
    private int  age3;
    @ApiModelProperty(example = "22")
    // @ApiModelProperty
    private long  age4;
}
