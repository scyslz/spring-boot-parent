package com.lz;

import com.lz.model.Student;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * TestController..
 *
 * @author Lizhong
 * @date 2019/7/12
 */
@Api
@RestController
public class TestController {
    @ApiOperation("operation01")
    @ApiImplicitParams(
            {@ApiImplicitParam(name = "age1", dataType = "Integer")
            ,@ApiImplicitParam(name = "age10", dataType = "int" ,example = "10")
                    , @ApiImplicitParam(name = "age2", dataType = "Long", example = "02")
                    , @ApiImplicitParam(name = "age20", dataType = "long" ,example = "20")
                    , @ApiImplicitParam(name = "student", dataTypeClass = Student.class)
            })
    @GetMapping("/test")
    public String method01(
            @RequestParam(name = "age1") Integer age1,
            @RequestParam(name = "age10") int age10,
            @RequestParam(name = "age2") Long age2,
            @RequestParam(name = "age20") long age20,
            Student student
    ) {
        return "hello";
    }
}
