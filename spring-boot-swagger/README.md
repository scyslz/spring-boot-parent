### swagger 
开启`@EnableSwagger2`,config配置文件不是必要,扫描所有接口 'basic-error-controller
                       Basic Error Controller
                       
'
### 解决swagger java.lang.NumberFormatException: For input string
1. 使用model和参中有int,long 类型的加example; 参数是Long也需要加example
2. 通过日志级别屏蔽
```
 logging:
       level:
         io.swagger.models.parameters.AbstractSerializableParameter: error
 ``` 
3. 代码有问题
```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
    <exclusions>
        <exclusion>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-models</artifactId>
        </exclusion>
    </exclusions>
</dependency>

<!--swagger-models bug修复-->
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-models</artifactId>
    <version>1.5.22</version>
</dependency>
```