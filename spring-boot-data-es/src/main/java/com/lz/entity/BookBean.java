package com.lz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/**
 * UserEntity
 *
 * @date 2020/12/30 11:42
 * @author Lizhong
 */
@Document(indexName = "book")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookBean {
    @Id
    private String id;
    @Field(index = false)
    private String title;
    private String author;
    private String postDate;
}