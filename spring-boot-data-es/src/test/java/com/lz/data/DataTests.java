package com.lz.data;

import com.lz.entity.BookBean;
import com.lz.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SpringBootTest
public class DataTests {
    @Autowired
    BookRepository bookRepository;

    @Test
    public void save() {
        BookBean bookBean = new BookBean();
        bookBean.setAuthor("author");
        bookBean.setId("2");
        bookBean.setTitle("hello lz");
        bookBean.setPostDate("2020年12月30日");
        bookRepository.save(bookBean);
    }

    @Test
    public void find() {

        Page<BookBean> hello = bookRepository.findByTitle("hello", PageRequest.of(0, 10));
        Assertions.assertFalse(hello.getContent().isEmpty());
        hello.forEach(System.out::println);
    }



}

