package com.lz;


import com.boomsecret.protobuf.MessageUserLogin;
import com.lz.service.UserService;
import com.lz.utils.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

@RunWith(SpringRunner.class)
@SpringBootTest
// @Transactional
@Rollback(false)
public class ApplicationTests {
    @Autowired
    UserService userService;

    @Test
    public void contextLoads() {

        userService.m01();

    }


    @Test
    public void contextLoads02() {

        userService.m02();

    }

    @Test
    public void method01() {
        userService.update01();
    }

    @Test
    public void method02() {
        userService.update02();
    }

    @Test
    public void method03() {
        userService.update03();
    }

    @Test
    public void method04() {
        userService.update04();
    }

    @Test
    public void test() {
        try {
            URI uri = new URI("http", null, "127.0.0.1", 8080, "/demo/test", "", null);
            HttpPost request = new HttpPost(uri);
            MessageUserLogin.MessageUserLoginRequest.Builder builder = MessageUserLogin.MessageUserLoginRequest.newBuilder();
            builder.setUsername("tom");
            builder.setPassword("123456");
            HttpResponse response = HttpUtils.doPost(request, builder.build());
            MessageUserLogin.MessageUserLoginResponse messageUserLoginResponse = MessageUserLogin.MessageUserLoginResponse.parseFrom(response.getEntity().getContent());
            System.err.println(messageUserLoginResponse.getAccessToken());
        } catch (Exception e) {

        }
    }

    @Test
    public void test05(){
            userService.update05();
            // userService.mannule();
    }


    @Test
    public void test06() {
        userService.mannule();
    }
    // Require_NEW
//     ===================UserService===================
//     实际 事务:true
//     同步 事务:true
//     事务名称:RedisLock-Transaction
// ===================UserService===================
//     执行业务逻辑
//     com.lz.service.Service03.method01 begin
// ===================Service03===================
//     实际 事务:true
//     同步 事务:true
//     事务名称:com.lz.service.Service03.method01
// ===================Service03===================
//     com.lz.service.Service03.method01 end
//     外层有事务！
//     after complete
// ===================Service03===================
//     实际 事务:true
//     同步 事务:false
//     事务名称:com.lz.service.Service03.method01
// ===================Service03===================
//     异常
// ===================UserService===================
//     实际 事务:false
//     同步 事务:false
//     事务名称:null
// ===================UserService===================

// 同一个事物
// ===================UserService begin===================
//     实际 事务:true
//     同步 事务:true
//     事务名称:RedisLock-Transaction
// ===================UserService end===================
//     执行业务逻辑
//     com.lz.service.Service03.method01 begin
// ===================
//     Service03 begin===================
//     实际 事务:true
//     同步 事务:true
//     事务名称:RedisLock-Transaction
// ===================Service03 end===================
//     com.lz.service.Service03.method01 end
//     异常
//     外层有事务！
//     after complete
// ===================Service03 begin===================
//     实际 事务:true
//     同步 事务:false
//     事务名称:RedisLock-Transaction
// ===================Service03 end===================
//             ===================UserService begin===================
//     实际 事务:false
//     同步 事务:false
//     事务名称:null
//             ===================UserService end===================
}

