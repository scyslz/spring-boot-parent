package com.lz.controller;

import com.boomsecret.protobuf.MessageUserLogin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

public class TestController {

    @Controller
    public class test {
        @RequestMapping(value = "/demo/test", produces = {"application/x-protobuf","application/json"})
        @ResponseBody
        public MessageUserLogin.MessageUserLoginResponse getPersonProto(@RequestBody MessageUserLogin.MessageUserLoginRequest request) {
            MessageUserLogin.MessageUserLoginResponse.Builder builder = MessageUserLogin.MessageUserLoginResponse.newBuilder();
            builder.setAccessToken(UUID.randomUUID().toString()+"_res");
            builder.setUsername(request.getUsername()+"_res");
            return builder.build();
        }

    }
}
