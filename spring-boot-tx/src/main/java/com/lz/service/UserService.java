package com.lz.service;

import com.lz.entity.User;
import com.lz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private static DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
    private static String transactionName = "RedisLock-Transaction";
    @Autowired
    private  PlatformTransactionManager dataSourceTransactionManager;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService02 userService02;

    @Autowired
    private Service03 service03;

    @Transactional
    public void m01() {
        List<User> all = userRepository.findAll();
        System.out.println(all.size());
        userService02.method02();
        List<User> all02 = userRepository.findAll();
        System.out.println(all02.size());

    }

    @Transactional
    public void m02() {
        List<User> all = userRepository.findAll();
        System.out.println(all.size());
        userService02.method03();
        List<User> all02 = userRepository.findAll();
        System.out.println(all02.size());
    }

    @Transactional
    public void update01() {
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("update01 ");
        userRepository.save(byId.get());

        userService02.innerExceptionUpdate01();

    }

    @Transactional
    public void update02() {
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("update02 ");
        userRepository.save(byId.get());
        userService02.innerExceptionUpdate02();
    }

    @Transactional
    public void update03() {
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("update01 ");
        userRepository.save(byId.get());

        userService02.update01();
        throw new RuntimeException();
    }

    @Transactional
    public void update04() {
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("update04 ");
        userRepository.save(byId.get());


        userService02.update02();
        throw new RuntimeException();
    }

    // 注册事务回调
    @Transactional
    public void update05() {
        service03.method01();
        service03.method01();
    }

    public void mannule() {
        transactionDefinition.setName(transactionName);
        TransactionStatus transaction = dataSourceTransactionManager.getTransaction(transactionDefinition);
        printf();
        try {
            System.out.println("执行业务逻辑");
            service03.method01();
            int j=1/0;
            dataSourceTransactionManager.commit(transaction);
        } catch (Exception e) {
            System.out.println("异常");
            dataSourceTransactionManager.rollback(transaction);
            throw e;
        }finally {
            printf();
        }
    }

    public void printf(){
        System.out.println("===================UserService begin===================");
        System.out.println("实际 事务:" + TransactionSynchronizationManager.isActualTransactionActive());
        System.out.println("同步 事务:" +TransactionSynchronizationManager.isSynchronizationActive());
        System.out.println("事务名称:" +TransactionSynchronizationManager.getCurrentTransactionName());
        System.out.println("===================UserService end===================");
    }

}