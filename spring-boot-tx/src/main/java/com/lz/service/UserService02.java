package com.lz.service;

import com.lz.entity.User;
import com.lz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService02 {
    @Autowired
    private UserRepository userRepository;





    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void method02() {
        List<User> all = userRepository.findAll();
        System.out.println(all.size());
    }

    @Transactional(propagation = Propagation.NESTED)
    public void method03() {
        List<User> all = userRepository.findAll();
        System.out.println(all.size());
    }
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void innerExceptionUpdate01(){
        throw  new RuntimeException();
    }
    @Transactional(propagation = Propagation.NESTED)
    public void innerExceptionUpdate02(){
        throw  new RuntimeException();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void update01(){
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("inner-new ");
        userRepository.save(byId.get());
    }
    @Transactional(propagation = Propagation.NESTED)
    public void update02(){
        Optional<User> byId = userRepository.findById(1);
        byId.get().setNickName("inner-nested ");
        userRepository.save(byId.get());
    }


}