package com.lz.service.jpa;

/**
 * UserDto..
 *
 * @author Lizhong
 * @date 2020/10/27
 */
public interface UserDto {
    public String getUserId() ;

    public String getUsername() ;
}
