package com.lz.repository;

import com.lz.entity.User;
import com.lz.service.jpa.UserDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, QuerydslPredicateExecutor<User> {


    @Query("select u from  user")
    UserDto findAllWithDto();
}