package com.lz;

import com.lz.entity.User;
import com.lz.repository.UserRepository;
import com.lz.service.jpa.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * DtoTest..
 *
 * @author Lizhong
 * @date 2020/10/27
 */
@RunWith(SpringRunner.class)
@DataJpaTest
// @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DtoTest {
    @Autowired
    UserRepository  userRepository;


    @Rollback(value = false)
    @Transactional
    @Before
    public   void init(){
        User user = new User();
        user.setNickName("li");
        user.setPassword("123");
        user.setBirthday(new Date());
        userRepository.save(user);
    }

    @Test
    public void method01(){
        UserDto allWithDto = userRepository.findAllWithDto();
        System.out.println(allWithDto.getUserId()+":"+allWithDto.getUsername());
    }
}
