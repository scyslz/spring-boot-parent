package com.lz;


import com.lz.entity.User;
import com.lz.service.UserService;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
// @Transactional
// @Rollback(false)
public class ApplicationTests {
    @Autowired
    UserService userService;

    @Test
    public void contextLoads() {

        long l = userService.countByNickNameLike("li");
        System.out.println(l);

        QueryResults<
                User> allPage = userService.findAllPage(PageRequest.of(0, 1));
        System.out.println(allPage.getTotal() + "," + allPage.getResults().size());

        // List<User> l1 = userService.findByUserPropertiesGroupByUIndex("l", null, null, null, null);
        // System.out.println(l1.size());

        List<UserDTO> allUserDto = userService.findAllUserDto(PageRequest.of(0, 1));
        System.out.println(allUserDto.size());

        List<Tuple> userAndtudent = userService.findUserAndtudent();
        System.out.println(userAndtudent.size());
    }


}

