package com.lz;


import com.yy.example.Application;
import com.yy.example.bean.Permission;
import com.yy.example.bean.Role;
import com.yy.example.bean.User;
import com.yy.example.dao.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =Application.class)
@Transactional
@Rollback(false)
public class SpringApplicationTests {

    @Autowired
    UserDao userDao;

    @Rollback(value = false)
    @Test
    @Transactional
    public void contextLoads() {
        User user = new User();
        user.setUsername("admin");
        user.setPassword("3c928bf98162167d4d7d8c1f52ef98d0");
        Role role = new Role();
        role.setName("ROLE_ADMIN");
        Permission permission = new Permission();
        permission.setMethod("all");
        permission.setPermissionUrl("/**");
        Permission permission0 = new Permission();
        permission0.setMethod("get");
        permission0.setPermissionUrl("/**");
        role.setPermissions(Arrays.asList(permission,permission0));
        user.setRole(Arrays.asList(role));
        userDao.save(user);

    }

}

