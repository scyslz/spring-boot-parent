package com.yy.example.bean;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import java.util.Collection;
import java.util.List;

@Data
@Entity
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String cnname;
    private String username;

    private String password;
    @Transient
    private List<? extends GrantedAuthority> authorities;

    @ManyToMany(cascade = CascadeType.PERSIST ,fetch = FetchType.EAGER)
    private List<Role> role;


    @Override

    public boolean isAccountNonExpired() {
        return true;
    }

    @Override

    public boolean isAccountNonLocked() {
        return true;
    }

    @Override

    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override

    public boolean isEnabled() {
        return true;
    }


    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}