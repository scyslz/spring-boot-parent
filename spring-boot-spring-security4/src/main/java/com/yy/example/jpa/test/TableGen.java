package com.yy.example.jpa.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * TableGen..
 *
 * @author Lizhong
 * @date 2019/7/23
 */
@Component
public class TableGen {
    @Value("${table.name}")
    private String suffix;



    public String getSuffix() {
        return suffix+"_"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM"));
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }





}
