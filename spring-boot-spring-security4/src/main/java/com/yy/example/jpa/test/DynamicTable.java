package com.yy.example.jpa.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * DynamicTable..
 *
 * @author Lizhong
 * @date 2019/7/23
 */
@Table(name = "#{tableGen.suffix}")
// @Table(name = "dynamic")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Where(clause = "del_flag = true")
public class DynamicTable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String name ;
    private  String addr ;
    boolean delFlag =true;
}
