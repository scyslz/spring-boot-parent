package com.yy.example.jpa.test;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * DynamicRepository..
 *
 * @author Lizhong
 * @date 2019/7/23
 */

@Repository
public interface DynamicRepository extends JpaRepository<DynamicTable,Long > ,JpaSpecificationExecutor<DynamicTable> {
}
