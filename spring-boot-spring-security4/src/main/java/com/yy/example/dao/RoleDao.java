package com.yy.example.dao;

import com.yy.example.bean.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * RoleDao..
 *
 * @author Lizhong
 * @date 2019/7/11
 */
public interface RoleDao   extends JpaRepository<Role,Integer> {
}
