package com.yy.example.dao;


import com.yy.example.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {


    User save(User user);

    User findByUsername(String userName);
}