package com.yy.example.service;

import com.yy.example.bean.User;
import com.yy.example.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	public User getById(Integer id) {
		User user = userDao.findById(id).get();
		return user;
	}
	

}