package com.lz.route;

import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;


/**
 *  自定义WebFlux配置 {@link org.springframework.web.reactive.config.WebFluxConfigurer }
 *
 * @author: li
 * @date: 2019/1/10
 */
public class DefaultWebFluxConfig implements WebFluxConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/person");


    }
}
