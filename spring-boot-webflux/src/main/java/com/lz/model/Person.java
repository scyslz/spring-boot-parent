package com.lz.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Title: Person
 * @ClassName: com.lz.model.Person.java
 * @Description: // TODO
 *
 * @Author by: li
 * @Date:  2019/1/9 14:07
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
public class Person {
    private  String name;
    private  int age;
}
