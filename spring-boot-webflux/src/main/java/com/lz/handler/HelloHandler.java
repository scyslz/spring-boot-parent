package com.lz.handler;

import com.lz.model.Person;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * @Classname: com.lz.handler.HelloHandler
 * @Description: TODO
 *
 * @Date: 2019/1/9 13:39
 * @Created by: li
 * @Version: V1.0
 */
@Component
public class HelloHandler {

    public Mono<ServerResponse> getPerson(ServerRequest serverRequest) {
        Person person = new Person("张三", 19);
        Flux<Person> people = Flux.just(person, new Person("李四", 25));
        return ok().contentType(APPLICATION_JSON).body(people, Person.class);
    }

    public Mono<ServerResponse> listPeople(ServerRequest serverRequest) {
        Person person = new Person("张三", 19);
        Flux<Person> people = Flux.just(person, new Person("李四", 25));
        return ok().contentType(APPLICATION_JSON).body(people, Person.class);

    }

    public Mono<ServerResponse> createPerson(ServerRequest serverRequest) {

        return null;
    }
}
