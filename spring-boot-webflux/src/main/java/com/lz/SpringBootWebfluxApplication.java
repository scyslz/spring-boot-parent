package com.lz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @Title: SpringBootWebfluxApplication
 * @ClassName: com.lz.SpringBootWebfluxApplication.java
 * @Description: // TODO
 *
 * @Author by: li
 * @Date:  2019/1/9 14:07
 * @Version: V1.0
 */
@SpringBootApplication
public class SpringBootWebfluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootWebfluxApplication.class, args);
    }


}

