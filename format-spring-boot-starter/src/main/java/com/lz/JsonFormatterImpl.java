package com.lz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JsonFormatterImpl..
 *
 * @author Lizhong
 * @date 2019/9/6
 */
public class JsonFormatterImpl implements Formatter {
    private final ObjectMapper objectMapper;


    public JsonFormatterImpl() {
        objectMapper = new ObjectMapper();
    }

    public JsonFormatterImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String format(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return  null;
    }
}
