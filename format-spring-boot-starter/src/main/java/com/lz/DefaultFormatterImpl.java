package com.lz;

/**
 * DefaultFormatterImpl..
 *
 * @author Lizhong
 * @date 2019/9/6
 */

public class DefaultFormatterImpl implements Formatter {
    @Override
    public String format(Object o) {
        return String.valueOf(o);

    }
}
