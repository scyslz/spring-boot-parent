package com.lz;

/**
 * Formatter..
 *
 * @author Lizhong
 * @date 2019/9/6
 */
public interface Formatter {
    String format(Object o);
}
