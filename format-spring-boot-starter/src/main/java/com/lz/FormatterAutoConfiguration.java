package com.lz;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.swing.text.DefaultFormatter;

/**
 * FormatterAutoConfiguration..
 *
 * @author Lizhong
 * @date 2019/9/6
 */
@Configuration
@AutoConfigureAfter(JacksonAutoConfiguration.class)
public class FormatterAutoConfiguration {
    // @Bean
    // @ConditionalOnMissingClass("com.fasterxml.jackson.databind.ObjectMapper")
    // public Formatter method01() {
    //     return new DefaultFormatterImpl();
    // }
    // @Bean(name = "ObjectJsonFormatter")
    // @ConditionalOnBean(ObjectMapper.class)
    // public Formatter objectJsonFormatter(ObjectMapper objectMapper) {
    //     return new JsonFormatterImpl(objectMapper);
    // }
    //
    // @Bean(name = "JsonFormatter")
    // @ConditionalOnClass(name = "com.fasterxml.jackson.databind.ObjectMapper")
    // @ConditionalOnMissingBean(name = "com.fasterxml.jackson.databind.ObjectMapper")
    // public Formatter jsonFormatter() {
    //     return new JsonFormatterImpl();
    // }

    /**
     * 构建 {@link DefaultFormatter} Bean
     *
     * @return {@link DefaultFormatter}
     */
    @Bean
    @ConditionalOnMissingClass(value = "com.fasterxml.jackson.databind.ObjectMapper")
    public Formatter defaultFormatter() {
        return new DefaultFormatterImpl();
    }

    /**
     * JSON 格式 {@link Formatter} Bean
     *
     * @return {@link JsonFormatter}
     */
    @Bean
    @ConditionalOnClass(name = "com.fasterxml.jackson.databind.ObjectMapper")
    @ConditionalOnMissingBean(type = "com.fasterxml.jackson.databind.ObjectMapper")
    public Formatter jsonFormatter() {
        return new JsonFormatterImpl();
    }

    @Bean
    @ConditionalOnBean(ObjectMapper.class)
    public Formatter objectMapperFormatter(ObjectMapper objectMapper) {
        return new JsonFormatterImpl(objectMapper);
    }

}
