package com.lz;

import com.lz.servlet.ProgramServlet2;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;

/**
 * ds..
 *
 * @author Lizhong
 * @date 2019/4/9
 */


public class Tomcat6FilterConfigurator implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext sc = event.getServletContext();

        ServletRegistration.Dynamic program = sc.addServlet("program", ProgramServlet2.class);
        program.addMapping("/program");

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // TODO Auto-generated method stub

    }

}