package com.lz.security;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * MySecurityServlet.. {@link ServletSecurity }
 *
 * @author Lizhong
 * @date 2019/4/14
 */
// 一、无任何约束，直接访问
// @ServletSecurity
//     二、在针对方法
/*
@ServletSecurity(
        @HttpConstraint(
                // transportGuarantee = ServletSecurity.TransportGuarantee.CONFIDENTIAL // 开启https 转发至8443端口
                //  value = ServletSecurity.EmptyRoleSemantic.DENY // 直接拒绝，默认=permit
                // rolesAllowed ={"user"} // 基于角色认证
        ))
*/

/*@ServletSecurity(httpMethodConstraints = {
        // 三、限制GET POST方法，仅有rolesAllowed。其他方法忽略
        @HttpMethodConstraint(value = "POST", rolesAllowed = "R1",
                transportGuarantee = ServletSecurity.TransportGuarantee.CONFIDENTIAL)
})*/
/*@ServletSecurity(
        // 四、所有请求都需要Role,除了POST
        value = @HttpConstraint(rolesAllowed = "user"),
        httpMethodConstraints = {
                        @HttpMethodConstraint(value = "POST")
        })*/
@ServletSecurity(
        // 四、所有请求都需要Role,除了POST
        value = @HttpConstraint(rolesAllowed = "member"),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "POST")
        })


@WebServlet("/security01")
public class MySecurityServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.getWriter().write(req.getMethod() + "\t" + "My Security Servlet ");
    }
}
