package com.lz.security;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * LoginServlet..
 *
 * @author Lizhong
 * @date 2019/4/15
 */

@WebServlet("/login")
// @ServletSecurity(value = @HttpConstraint(rolesAllowed = {"manager", "member"}))
public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/security01");
        dispatcher.forward(req, resp);
    }
}
