package com.lz.security;

import javax.servlet.ServletException;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * MySecurityServlet.. xml配置式 {@link ServletSecurity }
 *
 * @author Lizhong
 * @date 2019/4/14
 */

@WebServlet("/security02")
public class MySecurityServlet02 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.getWriter().write(req.getMethod()+ "\t"+ "My Security Servlet ");
    }
}
