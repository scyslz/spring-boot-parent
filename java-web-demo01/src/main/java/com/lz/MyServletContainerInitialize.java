package com.lz;

import com.lz.servlet.ProgramServlet2;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

/**
 * MyServletContainerInitialize.. 用户模块初始化未成功过,唯有单独任务或maven打包方可成功
 *
 * mmp：注意
 * 自定义MyServletContainerInitialize不生效，原因
 * 改造为maven，添加servlet api依赖
 * 1.标记web目录为resource
 * 2.删除编译的class ，                   查看是否编译类
 * 3.删除java-web-demo01-1.0-SNAPSHOT     重新编译看与class目录下是否相同
 *
 *
 *
 * 草 ： web.xml有内容也能加载，日了狗，可能是电脑没重启
 *
 *
 * 同理 web.xml的方式优先注解
 *
 * @author Lizhong
 * @date 2019/4/9
 */
// @HandlesTypes({ HttpServlet.class,Filter.class,ProgramServlet2.class,Servlet.class})
public class MyServletContainerInitialize
        implements ServletContainerInitializer
{
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        System.out.println("开始初始化 initialize **");
        ServletRegistration.Dynamic program = servletContext.addServlet("program", ProgramServlet2.class);
        program.addMapping("/program");
    }
}
