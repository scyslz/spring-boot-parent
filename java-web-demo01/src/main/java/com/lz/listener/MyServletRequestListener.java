package com.lz.listener;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import java.time.LocalDate;

/**
 * MyServletRequestListener..
 *
 * @author Lizhong
 * @date 2019/4/12
 */
@WebListener
public class MyServletRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        // 一次请求后就被调用
        System.out.println(LocalDate.now()+"\t MyServletRequestListener 结束了");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println(LocalDate.now()+"\t MyServletRequestListener 开始了");
        ServletRequest servletContext = sre.getServletRequest();
        String remoteHost = servletContext.getRemoteHost();
        System.out.println(remoteHost);
    }
}
