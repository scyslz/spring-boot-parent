package com.lz.listener;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * MyServlet..
 *
 * @author Lizhong
 * @date 2019/4/12
 */

public class MyServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String x = "通过ServletContextListener注册:MyServlet";
        System.out.println(x);
        resp.getWriter().write(x);
    }
}
