package com.lz.service;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * HelloService.. 定时调用任务
 *
 * @author Lizhong
 * @date 2019/7/10
 */

@Service("helloService")
public class HelloService {
    public void sayHello() {
        System.out.println("Hello！ 张三 " + LocalDateTime.now());
    }

    public boolean sayHello2(String name) {
        System.out.println("Hello！ " + name + LocalDateTime.now());
        return  true;
    }
}
