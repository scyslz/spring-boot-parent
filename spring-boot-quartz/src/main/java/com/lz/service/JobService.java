package com.lz.service;

import com.lz.dao.JobEntityRepository;
import com.lz.entity.JobEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by EalenXie on 2018/6/4 14:25
 */
@Service
public class JobService {

    @Autowired
    private JobEntityRepository repository;

    //通过Id获取Job
    public JobEntity getJobEntityById(Integer id) {
        return repository.getById(id);
    }

    //从数据库中加载获取到所有Job
    public List<JobEntity> loadJobs() {
        return repository.findAll();
    }


}
