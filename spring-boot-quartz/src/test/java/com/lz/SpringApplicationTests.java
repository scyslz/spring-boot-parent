package com.lz;


import com.lz.entity.JobEntity;
import com.lz.web.JobController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuartzApplication.class)
public class SpringApplicationTests {

    @Autowired
    JobController jobController;

    @Rollback(value = false)
    @Test
    @Transactional
    public void contextLoads() {
        JobEntity jobEntity = new JobEntity();
        jobEntity.setCron("0/2 * * * * ? ");
        jobEntity.setMethod("sayHello");
        jobEntity.setBeanClass("helloService");
        jobEntity.setName("helloServiceName4");
        jobEntity.setJobGroup("service");
        jobEntity.setStatus("OPEN");
        jobController.addJob(jobEntity);
    }

}

