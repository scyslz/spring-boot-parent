package com.lz;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

/**
 * MyServletInitialize.. 用户模块初始化未成功过,唯有单独任务或maven打包方可成功
 *
 * @author Lizhong
 * @date 2019/4/9
 */
// @HandlesTypes({ HttpServlet.class,Filter.class,ProgramServlet2.class,Servlet.class})
public class MyServletInitialize implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        System.out.println("开始初始化 initialize");
        ServletRegistration.Dynamic program = servletContext.addServlet("program", ProgramServlet2.class);
        program.addMapping("/program");
    }
}
