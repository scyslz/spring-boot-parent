package com.lz;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * HelloFilter..
 *
 * @author Lizhong
 * @date 2019/4/23
 */
@WebFilter(servletNames = "helloServlet")
public class HelloFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("我被初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("什么都不干");
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("filter 死了");
    }
}
