package com.lz;


import com.yy.example.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =Application.class)
@Rollback(false)
public class SpringApplicationTests {


    @Rollback(value = false)
    @Test
    // @Transactional
    public void contextLoads() {

    }

}

