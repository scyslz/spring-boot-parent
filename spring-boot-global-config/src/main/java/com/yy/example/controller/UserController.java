package com.yy.example.controller;

import com.yy.example.repository.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/users")
@RestController
public class UserController {


    @RequestMapping(method = RequestMethod.GET)
    public Object list(HttpServletRequest request) {
        return ResponseEntity.ok(new User());
    }



}