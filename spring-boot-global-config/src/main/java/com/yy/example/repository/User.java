package com.yy.example.repository;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

/**
 * User..
 *
 * @author Lizhong
 * @date 2020/4/27
 */
public class User  {

    private LocalDateTime localDateTime = LocalDateTime.now();
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime annotation=LocalDateTime.now();

    private  LocalDateTime customeAnnotation=LocalDateTime.now();


    public LocalDateTime getAnnotation() {
        return annotation;
    }

    public void setAnnotation(LocalDateTime annotation) {
        this.annotation = annotation;
    }

    public LocalDateTime getCustomeAnnotation() {
        return customeAnnotation;
    }

    public void setCustomeAnnotation(LocalDateTime customeAnnotation) {
        this.customeAnnotation = customeAnnotation;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
